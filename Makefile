all:
	@echo "make venv - create virtual environment (do this once before running the server)"
	@echo "make run - run the frontend"
	@echo "make server - run the backend"
	@echo "make add - add all files to git"
	@echo "make pull - pull from remote gitlab"

run:
	cd ./frontend && npm run dev

venv: venv/touchfile

venv/touchfile: 
	./backend/requirements.txt
	test -d venv || virtualenv venv
	. venv/bin/activate; pip install -Ur ./backend/requirements.txt
	touch venv/touchfile

server:
	. venv/bin/activate
	cd ./backend && python main.py

#add all then show status
add:
	git add -A
	git status

# pull from remote gitlab
pull:
	git pull
	git status