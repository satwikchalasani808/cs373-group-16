// rollup.config.js
import commonjs from '@rollup/plugin-commonjs';

export default {
  input: 'path/to/your/test-file.js',
  output: {
    file: 'bundle.js',
    format: 'cjs',
  },
  plugins: [commonjs()],
};
