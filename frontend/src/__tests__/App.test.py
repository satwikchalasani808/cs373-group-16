# #  // import { render, screen, cleanup, act } from "@testing-library/svelte";
# #  // import mockAboutFetch from "../mocks/mockFetch";
# #  // import About from "../routes/about/+page.svelte";

# #  // test("should properly fetch and set commits, issues", async () => {
# #    // jest.spyOn(global, "fetch").mockImplementation(mockAboutFetch);

# #    // await act(async () => {
# #    //   render(<About />);
# #    // }); 
# #    // expect(screen.getByText("Total Commits: 25Total Issues")).toBeInTheDocument();
# #  // }); 

#  # Import necessary libraries and setup
#  import '@testing-library/jest-dom'
#  from '@testing-library/svelte' import render
 
#  # Import the Svelte component to be tested
#  from '../components/DeveloperCard.svelte' import DeveloperCard
 
#  # Configure fake timers
#  jest.useFakeTimers()
 
#  # Tests for the Navbar component
#  describe('Navbar component', () => {
#      # Test 1: Renders the navbar with logo and links when passed as props
#      test('renders the navbar with logo and links when passed in as props', async () => {
#          # Define logo source
#          logoSrc = "https://www.care-hub.me/"
         
#          # Render the Navbar component with the provided logo source
#          await render(Nav, { logo: { src: logoSrc } })
         
#          # Assertion: Check if img has correct source attribute
#          expect(screen.getByRole('img')).toHaveAttribute('src', logoSrc)
         
#          # Assertion: Check if at least one link is rendered
#          expect(screen.getAllByRole('link').length).toBeGreaterThanOrEqual(1)
#      })
 
#      # Test 2: Renders about link's text
#      test("renders about link's text", () => {
#          # Define expected text for the about link
#          expectedText = "About"
         
#          # Render the Navbar component
#          render(Nav)
         
#          # Assertion: Check if the about link's text is rendered
#          expect(screen.getByRole('link', { name: expectedText })).toHaveTextContent(expectedText)
#      })
 
#      # Test 3: Renders demographics link's text
#      test("renders demographics link's text", () => {
#          # Define expected text for the demographics link
#          expectedText = "Demographics"
         
#          # Render the Navbar component
#          render(Nav)
         
#          # Assertion: Check if the demographics link's text is rendered
#          expect(screen.getByRole('link', { name: expectedText })).toHaveTextContent(expectedText)
#      })
 
#      # Test 4: Renders hospitals link's text
#      test("renders hospitals link's text", () => {
#          # Define expected text for the hospitals link
#          expectedText = "Hospitals"
         
#          # Render the Navbar component
#          render(Nav)
         
#          # Assertion: Check if the hospitals link's text is rendered
#          expect(screen.getByRole('link', { name: expectedText })).toHaveTextContent(expectedText)
#      })
 
#      # Test 5: Renders organizations link's text
#      test("renders organizations link's text", () => {
#          # Define expected text for the organizations link
#          expectedText = "Organizations"
         
#          # Render the Navbar component
#          render(Nav)
         
#          # Assertion: Check if the organizations link's text is rendered
#          expect(screen.getByRole('link', { name: expectedText })).toHaveTextContent(expectedText)
#      })
#  })
 
#  # Tests for the Splash page and about
#  describe("Splash page and about testing", () =>{
#      # Test 6: Renders splash page component
#      test("renders splash page component", () => {
#          # Render the SplashPage component
#          render(SplashPage)
         
#          # Assertion: Check if the splash page component is rendered
#          expect(screen.getByTestId("splash-page")).toBeInTheDocument()
#      })
 
#      # Test 7: Redirects to home when clicked on 'Learn More' button from Splash Page
#      test("redirects to home when clicked on 'Learn More' button from Splash Page", async () => {
#          # Mock scrollTo function
#          window.scrollTo = jest.fn()
         
#          # Render the SplashPage component
#          render(SplashPage)
         
#          # Simulate click on 'Learn More' button
#          userEvent.click(screen.getByTestId("learnMoreBtn"))
         
#          # Assertion: Check if window location is assigned to '/home' after click
#          await waitFor(() => expect(window.location.assign).toHaveBeenCalledWith("/home"))
#      })
#  })
 
#  # Tests for the Tool Card component
#  describe('Tool Card component', () => {
#      # Test 9: Renders tool card with correct props
#      test("renders tool card with correct props", () => {
#          # Define props for the ToolCard component
#          props = {
#              imageUrl: '/path/to/image.png',
#              name: 'Svelte'
#          }
     
#          # Render the ToolCard component with the defined props
#          getByAltText = render(ToolCard, { props }).getByAltText
     
#          # Assertion: Check if the component renders with the correct props
#          expect(getByAltText('Svelte')).toBeInTheDocument()
#      })
#  })
 