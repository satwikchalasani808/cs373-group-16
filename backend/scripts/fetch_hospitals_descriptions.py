from requests_html import HTMLSession
from bs4 import BeautifulSoup
from readability import Document
import psycopg2
import re


headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/110.0'}
db_host = "localhost"
db_port = "5432"
db_name = "postgres"

connection = psycopg2.connect(
            host=db_host,
            port=db_port,
            dbname=db_name
        )
cursor = connection.cursor()

import signal
def handler(signum, frame):
    connection.commit()
    cursor.close()
    connection.close()

signal.signal(signal.SIGTSTP, handler)

def fetch_hospital_paragraph(url: str):
    session = HTMLSession()
    response = session.get(url + "about", headers=headers)
    response.html.render(sleep=1)

    # Parse HTML content using BeautifulSoup
    soup = BeautifulSoup(response.text, 'html.parser')

    # Find first paragraph long enough
    curr_p = soup.find('p', recursive=True)
    content = ""
    while len(content) < 100 and curr_p is not None:
        content = curr_p.text
        curr_p = curr_p.find_next('p', recursive=True)

    if content and len(content) >= 100:
        return content
    else:
        return None

if __name__ == "__main__":
    try:
        cursor.execute("SELECT url FROM hospitals WHERE description IS NULL AND url IS NOT NULL ORDER BY random() LIMIT 10")
        links = cursor.fetchall()

        while links:
            for i, link in enumerate(links):
                link = link[0]
                try: 
                    desc = fetch_hospital_paragraph(link)
                except:
                    print(f"{i}/{len(links)}: Failed to fetch for {link}")
                    continue
                if not desc:
                    print(f"{i}/{len(links)}: No description for {link}")
                    continue
                cursor.execute("UPDATE hospitals SET description = %s WHERE url = %s", (desc, link))
                print(f"{i}/{len(links)}: Updated {link} with description")
            connection.commit()
            cursor.execute("SELECT url FROM hospitals WHERE description IS NULL AND url IS NOT NULL ORDER BY random() LIMIT 10")
            hospitals = cursor.fetchall()
          
        cursor.close()
        connection.close()
    except KeyboardInterrupt:
        connection.commit()
        cursor.close()
        connection.close()